import { makeStyles } from '@material-ui/styles';

// eslint-disable-next-line import/prefer-default-export
export const useStyles = makeStyles(theme => ({
  button: {
    margin: theme.spacing.unit,
    width: 190,
    height: 40,
    borderRadius: 4,
    backgroundColor: '#2980b9',
    color: theme.palette.common.white,
    textTransform: 'none',
    fontSize: 17,
    fontWeight: 400,
  },
}));
