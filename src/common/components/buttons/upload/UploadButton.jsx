/* eslint-disable jsx-a11y/label-has-for */
import React from 'react';
import PropTypes from 'prop-types';

import Button from '@material-ui/core/Button';

import * as hooks from './hooks';

function UploadButton({
  label,
  id,
  onChange,
}) {
  const classes = hooks.useStyles();

  return (
    <React.Fragment>
      <input
        accept="image/*"
        className={classes.input}
        id={id}
        multiple
        type="file"
        onChange={onChange}
      />
      <label htmlFor={id}>
        <Button variant="contained" component="span" className={classes.button}>
          {label}
        </Button>
      </label>
    </React.Fragment>
  );
}

UploadButton.propTypes = {
  label: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default UploadButton;
