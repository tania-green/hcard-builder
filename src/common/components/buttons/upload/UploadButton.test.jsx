import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import { stub } from 'sinon';

import Button from '@material-ui/core/Button';

import UploadButton from './UploadButton';
import * as hooks from './hooks';

function setup(specProps) {
  const defaultProps = {};

  const props = {
    ...defaultProps,
    ...specProps,
  };
  const enzymeWrapper = shallow(<UploadButton {...props} />);

  return {
    props,
    enzymeWrapper,
  };
}

describe('UploadButton', () => {
  before(() => {
    stub(hooks, 'useStyles').returns({});
  });

  after(() => {
    hooks.useStyles.restore();
  });

  it('should render self and have first React.Fragment', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.first().is(React.Fragment)).to.equal(true);
  });

  it('should render one Button', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.find(Button)).length(1);
  });

  it('should render one label', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.find('label')).length(1);
  });

  it('should render one input', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.find('input')).length(1);
  });
});
