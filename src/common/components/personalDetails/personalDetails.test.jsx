import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import { stub } from 'sinon';

import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';

import PersonalDetails from './PersonalDetails';
import TextInput from '../textInput/TextInput';
import * as hooks from './hooks';

function setup(specProps) {
  const defaultProps = {};

  const props = {
    ...defaultProps,
    ...specProps,
  };
  const enzymeWrapper = shallow(<PersonalDetails {...props} />);

  return {
    props,
    enzymeWrapper,
  };
}

describe('PersonalDetails', () => {
  before(() => {
    stub(hooks, 'useStyles').returns({});
  });

  after(() => {
    hooks.useStyles.restore();
  });

  it('should render self and have first div', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.first().is('div')).to.equal(true);
  });

  it('should render one Typography', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.find(Typography)).length(1);
  });

  it('should render one Divider', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.find(Divider)).length(1);
  });

  it('should render four TextInputs', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.find(TextInput)).length(4);
  });
});
