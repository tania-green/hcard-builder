import React from 'react';
import PropTypes from 'prop-types';

import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';

import TextInput from '../textInput/TextInput';

import * as hooks from './hooks';

function PersonalDetails({
  name,
  surname,
  email,
  phone,
  onChange,
}) {
  const classes = hooks.useStyles();

  return (
    <div className={classes.root}>
      <Typography variant="h2">
          PERSONAL DETAILS
      </Typography>
      <Divider className={classes.divider} />
      <TextInput
        id="name-input"
        label="NAME"
        value={name}
        onChange={onChange}
      />
      <TextInput
        id="surname-input"
        label="SURNAME"
        value={surname}
        onChange={onChange}
      />
      <TextInput
        id="email-input"
        label="EMAIL"
        email={email}
        onChange={onChange}
      />
      <TextInput
        id="phone-input"
        label="PHONE"
        phone={phone}
        onChange={onChange}
      />
    </div>
  );
}

PersonalDetails.propTypes = {
  onChange: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  surname: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired,
  phone: PropTypes.string.isRequired,
};

export default PersonalDetails;
