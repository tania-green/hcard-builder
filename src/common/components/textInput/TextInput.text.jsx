/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';

import { expect } from 'chai';
import { shallow } from 'enzyme';
import { stub } from 'sinon';

import InputBase from '@material-ui/core/InputBase';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';

import TextInput from './TextInput';
import * as hooks from './hooks';

function setup(specProps) {
  const defaultProps = {};

  const props = {
    ...defaultProps,
    ...specProps,
  };
  const enzymeWrapper = shallow(<TextInput {...props} />);

  return {
    props,
    enzymeWrapper,
  };
}

describe('PersonalDetails', () => {
  before(() => {
    stub(hooks, 'useStyles').returns({});
  });

  after(() => {
    hooks.useStyles.restore();
  });

  it('should render self and have first React.Fragment', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.first().is(React.Fragment)).to.equal(true);
  });

  it('should render one FormControl', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.find(FormControl)).length(1);
  });

  it('should render one InputLabel', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.find(InputLabel)).length(1);
  });

  it('should render one InputBase', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.find(InputBase)).length(1);
  });
});
