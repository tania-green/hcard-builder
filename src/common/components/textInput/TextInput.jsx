import React from 'react';
import PropTypes from 'prop-types';

import InputBase from '@material-ui/core/InputBase';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';

import * as hooks from './hooks';

function TextInput({
  label,
  id,
  onChange,
  value,
}) {
  const classes = hooks.useStyles();

  return (
    <React.Fragment>
      <FormControl className={classes.margin}>
        <InputLabel shrink htmlFor={id} className={classes.bootstrapFormLabel}>
          {label}
        </InputLabel>
        <InputBase
          id={id}
          classes={{
            root: classes.bootstrapRoot,
            input: classes.bootstrapInput,
          }}
          value={value}
          onChange={onChange}
          name={id}
        />
      </FormControl>
    </React.Fragment>
  );
}

TextInput.propTypes = {
  label: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  value: PropTypes.string,
  onChange: PropTypes.func.isRequired,
};

TextInput.defaultProps = {
  value: '',
};

export default TextInput;
