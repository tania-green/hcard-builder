import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';

import CircularProgress from '@material-ui/core/CircularProgress';

import Loading from './Loading';

function setup(props) {
  const enzymeWrapper = shallow(<Loading {...props} />);

  return {
    props,
    enzymeWrapper,
  };
}

describe('Loading', () => {
  it('should render self and have CircularProgress', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.first().is(CircularProgress)).to.equal(true);
  });
});
