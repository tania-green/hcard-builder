import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import { stub } from 'sinon';

import Typography from '@material-ui/core/Typography';

import PersonalDetails from '../personalDetails/PersonalDetails';
import Address from '../address/Address';
import UploadButton from '../buttons/upload/UploadButton';
import SaveButton from '../buttons/save/SaveButton';
import HCardBuilder from './HCardBuilder';
import * as hooks from './hooks';

function setup(specProps) {
  const defaultProps = {};

  const props = {
    ...defaultProps,
    ...specProps,
  };
  const enzymeWrapper = shallow(<HCardBuilder {...props} />);

  return {
    props,
    enzymeWrapper,
  };
}

describe('HCardBuilder', () => {
  before(() => {
    stub(hooks, 'useStyles').returns({});
  });

  after(() => {
    hooks.useStyles.restore();
  });

  it('should render self and have first div', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.first().is('div')).to.equal(true);
  });

  it('should render one Typography', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.find(Typography)).length(1);
  });

  it('should render one PersonalDetails', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.find(PersonalDetails)).length(1);
  });

  it('should render one Address', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.find(Address)).length(1);
  });

  it('should render one UploadButton', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.find(UploadButton)).length(1);
  });

  it('should render one SaveButton', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.find(SaveButton)).length(1);
  });
});
