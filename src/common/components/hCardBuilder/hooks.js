import { makeStyles } from '@material-ui/styles';

// eslint-disable-next-line import/prefer-default-export
export const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    paddingTop: 44,
    paddingBottom: 44,
    paddingLeft: 40,
    paddingRight: 40,
    width: '100%',
  },
  buttonGroup: {
    paddingTop: 20,
  },
}));
