import React from 'react';
import PropTypes from 'prop-types';

import Typography from '@material-ui/core/Typography';

import PersonalDetails from '../personalDetails/PersonalDetails';
import Address from '../address/Address';
import UploadButton from '../buttons/upload/UploadButton';
import SaveButton from '../buttons/save/SaveButton';

import * as hooks from './hooks';

function HCardBuilder({
  onUpload,
  onSave,
  onChange,
  name,
  surname,
  email,
  phone,
  house,
  street,
  suburb,
  state,
  postcode,
  country,
}) {
  const classes = hooks.useStyles();

  return (
    <div className={classes.root}>
      <Typography variant="h1">
          hCard Builder
      </Typography>
      <PersonalDetails
        onChange={onChange}
        name={name}
        surname={surname}
        email={email}
        phone={phone}
      />
      <Address
        onChange={onChange}
        house={house}
        street={street}
        suburb={suburb}
        state={state}
        postcode={postcode}
        country={country}
      />
      <div className={classes.buttonGroup}>
        <UploadButton
          label="Upload Avatar"
          id="upload-avatar-button"
          onChange={onUpload}
        />
        <SaveButton
          label="Create hCard"
          id="create-hcard-button"
          onClick={onSave}
        />
      </div>
    </div>
  );
}

HCardBuilder.propTypes = {
  onUpload: PropTypes.func.isRequired,
  onSave: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  surname: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired,
  phone: PropTypes.string.isRequired,
  house: PropTypes.string.isRequired,
  street: PropTypes.string.isRequired,
  suburb: PropTypes.string.isRequired,
  state: PropTypes.string.isRequired,
  postcode: PropTypes.string.isRequired,
  country: PropTypes.string.isRequired,
};

export default HCardBuilder;
