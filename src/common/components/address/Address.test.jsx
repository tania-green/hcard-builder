import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';

import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';

import Address from './Address';
import TextInput from '../textInput/TextInput';

function setup(specProps) {
  const defaultProps = {
  };

  const props = {
    ...defaultProps,
    ...specProps,
  };
  const enzymeWrapper = shallow(<Address {...props} />);

  return {
    props,
    enzymeWrapper,
  };
}

describe('Address', () => {
  it('should render self and have first div', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.first().is('div')).to.equal(true);
  });

  it('should render one Typography', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.find(Typography)).length(1);
  });

  it('should render one Divider', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.find(Divider)).length(1);
  });

  it('should render six TextInputs', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.find(TextInput)).length(6);
  });
});
