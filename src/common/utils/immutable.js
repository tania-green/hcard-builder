export function updateObjectInArray(items, index, value) {
  return items.map((item, i) => {
    if (i !== index) {
      return item;
    }
    return {
      ...item,
      ...value,
    };
  });
}

export function insertItem(array, index, value) {
  return [
    ...array.slice(0, index),
    value,
    ...array.slice(index),
  ];
}

export function removeItem(array, index) {
  return [
    ...array.slice(0, index),
    ...array.slice(index + 1),
  ];
}
