export default {
  palette: {
    type: 'light',
    primary: {
      main: '#2c3e50',
      contrastText: '#ffffff',
      light: '#EBEBEB',
    },
    secondary: {
      main: '#d0d0d0',
      contrastText: '#ffffff',
    },
  },
  typography: {
    useNextVariants: true,
    h1: {
      width: '100%',
      fontSize: 28,
      color: '#2c3e50',
      fontFamily: 'Merriweather Sans',
      fontWeight: 800,
    },
    h2: {
      width: '100%',
      fontSize: 10,
      color: '#b0b8bc',
      fontFamily: 'Merriweather Sans',
      fontWeight: 700,
      paddingTop: 31,
    },
    h3: {
      width: '100%',
      fontSize: 12,
      color: '#2c3e50',
      fontFamily: 'Merriweather Sans',
      fontWeight: 400,
    },
    subtitle1: {
      textTransform: 'uppercase',
      fontSize: 16,
      color: '#95a5a6',
      fontFamily: 'Merriweather Sans, Roboto',
      fontWeight: 400,
    },
  },
};
