import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import { stub } from 'sinon';

import * as hooks from './hooks';

import HCardBuilderPage from './HCardBuilderPage';
import HCardBuilder from '../../common/components/hCardBuilder/HCardBuilder';
import HCardPreview from '../../common/components/hCardPreview/HCardPreview';

function setup(specProps) {
  const defaultProps = {
  };

  const props = {
    ...defaultProps,
    ...specProps,
  };
  const enzymeWrapper = shallow(<HCardBuilderPage {...props} />);

  return {
    props,
    enzymeWrapper,
  };
}

describe('HCardBuilderPage', () => {
  const setNameStub = stub();
  const setSurnameStub = stub();
  const setEmailStub = stub();
  const setPhoneStub = stub();
  const setHouseStub = stub();
  const setStreetStub = stub();
  const setSuburbStub = stub();
  const setStateStub = stub();
  const setPostcodeStub = stub();
  const setCountryStub = stub();
  const setAvatarStub = stub();

  before(() => {
    stub(hooks, 'useStyles').returns({});
    stub(hooks, 'useName').returns(['', setNameStub]);
    stub(hooks, 'useSurname').returns(['', setSurnameStub]);
    stub(hooks, 'useEmail').returns(['', setEmailStub]);
    stub(hooks, 'usePhone').returns(['', setPhoneStub]);
    stub(hooks, 'useHouse').returns(['', setHouseStub]);
    stub(hooks, 'useStreet').returns(['', setStreetStub]);
    stub(hooks, 'useSuburb').returns(['', setSuburbStub]);
    stub(hooks, 'useAddressState').returns(['', setStateStub]);
    stub(hooks, 'usePostcode').returns(['', setPostcodeStub]);
    stub(hooks, 'useCountry').returns(['', setCountryStub]);
    stub(hooks, 'useAvatar').returns(['', setAvatarStub]);
  });

  after(() => {
    hooks.useStyles.restore();
    hooks.useName.restore();
    hooks.useSurname.restore();
    hooks.useEmail.restore();
    hooks.usePhone.restore();
    hooks.useHouse.restore();
    hooks.useStreet.restore();
    hooks.useSuburb.restore();
    hooks.useAddressState.restore();
    hooks.usePostcode.restore();
    hooks.useCountry.restore();
    hooks.useAvatar.restore();
  });

  it('should render self and have first div', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.first().is('div')).to.equal(true);
  });

  it('should render one HCardBuilder', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.find(HCardBuilder)).length(1);
  });

  it('should render one HCardPreview', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.find(HCardPreview)).length(1);
  });

  it('onSave should call setName', () => {
    const { enzymeWrapper } = setup();
    enzymeWrapper.find(HCardBuilder).first().prop('onSave')();
    expect(setNameStub.calledWith('')).to.equal(true);
  });

  it('onSave should call setSurname', () => {
    const { enzymeWrapper } = setup();
    enzymeWrapper.find(HCardBuilder).first().prop('onSave')();
    expect(setSurnameStub.calledWith('')).to.equal(true);
  });

  it('onSave should call setEmail', () => {
    const { enzymeWrapper } = setup();
    enzymeWrapper.find(HCardBuilder).first().prop('onSave')();
    expect(setEmailStub.calledWith('')).to.equal(true);
  });

  it('onSave should call setPhone', () => {
    const { enzymeWrapper } = setup();
    enzymeWrapper.find(HCardBuilder).first().prop('onSave')();
    expect(setPhoneStub.calledWith('')).to.equal(true);
  });

  it('onSave should call setHouse', () => {
    const { enzymeWrapper } = setup();
    enzymeWrapper.find(HCardBuilder).first().prop('onSave')();
    expect(setHouseStub.calledWith('')).to.equal(true);
  });

  it('onSave should call setStreet', () => {
    const { enzymeWrapper } = setup();
    enzymeWrapper.find(HCardBuilder).first().prop('onSave')();
    expect(setStreetStub.calledWith('')).to.equal(true);
  });

  it('onSave should call setSuburb', () => {
    const { enzymeWrapper } = setup();
    enzymeWrapper.find(HCardBuilder).first().prop('onSave')();
    expect(setSuburbStub.calledWith('')).to.equal(true);
  });

  it('onSave should call setState', () => {
    const { enzymeWrapper } = setup();
    enzymeWrapper.find(HCardBuilder).first().prop('onSave')();
    expect(setStateStub.calledWith('')).to.equal(true);
  });

  it('onSave should call setPostcode', () => {
    const { enzymeWrapper } = setup();
    enzymeWrapper.find(HCardBuilder).first().prop('onSave')();
    expect(setPostcodeStub.calledWith('')).to.equal(true);
  });

  it('onSave should call setCountry', () => {
    const { enzymeWrapper } = setup();
    enzymeWrapper.find(HCardBuilder).first().prop('onSave')();
    expect(setCountryStub.calledWith('')).to.equal(true);
  });

  it('onSave should call setAvatar', () => {
    const { enzymeWrapper } = setup();
    enzymeWrapper.find(HCardBuilder).first().prop('onSave')();
    expect(setAvatarStub.calledWith('')).to.equal(true);
  });

  it('given e.target.name = name-input, onChange should call setName', () => {
    const { enzymeWrapper } = setup();
    const e = { target: { name: 'name-input', value: 'value' } };
    enzymeWrapper.find(HCardBuilder).first().prop('onChange')(e);
    expect(setNameStub.calledWith(e.target.value)).to.equal(true);
  });

  it('given e.target.name = name-input, onChange should not call setSurname', () => {
    const { enzymeWrapper } = setup();
    const e = { target: { name: 'name-input', value: 'value' } };
    enzymeWrapper.find(HCardBuilder).first().prop('onChange')(e);
    expect(setSurnameStub.calledWith(e.target.value)).to.equal(false);
  });

  it('given e.target.name = name-input, onChange should not call setEmail', () => {
    const { enzymeWrapper } = setup();
    const e = { target: { name: 'name-input', value: 'value' } };
    enzymeWrapper.find(HCardBuilder).first().prop('onChange')(e);
    expect(setEmailStub.calledWith(e.target.value)).to.equal(false);
  });

  it('given e.target.name = name-input, onChange should not call setPhone', () => {
    const { enzymeWrapper } = setup();
    const e = { target: { name: 'name-input', value: 'value' } };
    enzymeWrapper.find(HCardBuilder).first().prop('onChange')(e);
    expect(setPhoneStub.calledWith(e.target.value)).to.equal(false);
  });

  it('given e.target.name = name-input, onChange should not call setHouse', () => {
    const { enzymeWrapper } = setup();
    const e = { target: { name: 'name-input', value: 'value' } };
    enzymeWrapper.find(HCardBuilder).first().prop('onChange')(e);
    expect(setHouseStub.calledWith(e.target.value)).to.equal(false);
  });

  it('given e.target.name = name-input, onChange should not call setStreet', () => {
    const { enzymeWrapper } = setup();
    const e = { target: { name: 'name-input', value: 'value' } };
    enzymeWrapper.find(HCardBuilder).first().prop('onChange')(e);
    expect(setStreetStub.calledWith(e.target.value)).to.equal(false);
  });

  it('given e.target.name = name-input, onChange should not call setSuburb', () => {
    const { enzymeWrapper } = setup();
    const e = { target: { name: 'name-input', value: 'value' } };
    enzymeWrapper.find(HCardBuilder).first().prop('onChange')(e);
    expect(setSuburbStub.calledWith(e.target.value)).to.equal(false);
  });

  it('given e.target.name = name-input, onChange should not call setState', () => {
    const { enzymeWrapper } = setup();
    const e = { target: { name: 'name-input', value: 'value' } };
    enzymeWrapper.find(HCardBuilder).first().prop('onChange')(e);
    expect(setStateStub.calledWith(e.target.value)).to.equal(false);
  });

  it('given e.target.name = name-input, onChange should not call setPostcode', () => {
    const { enzymeWrapper } = setup();
    const e = { target: { name: 'name-input', value: 'value' } };
    enzymeWrapper.find(HCardBuilder).first().prop('onChange')(e);
    expect(setPostcodeStub.calledWith(e.target.value)).to.equal(false);
  });

  it('given e.target.name = name-input, onChange should not call setCountry', () => {
    const { enzymeWrapper } = setup();
    const e = { target: { name: 'name-input', value: 'value' } };
    enzymeWrapper.find(HCardBuilder).first().prop('onChange')(e);
    expect(setCountryStub.calledWith(e.target.value)).to.equal(false);
  });

  it('given e.target.name = surname-input, onChange should call setSurname', () => {
    const { enzymeWrapper } = setup();
    const e = { target: { name: 'surname-input', value: 'value' } };
    enzymeWrapper.find(HCardBuilder).first().prop('onChange')(e);
    expect(setSurnameStub.calledWith(e.target.value)).to.equal(true);
  });

  it('given e.target.name = email-input, onChange should call setEmail', () => {
    const { enzymeWrapper } = setup();
    const e = { target: { name: 'email-input', value: 'value' } };
    enzymeWrapper.find(HCardBuilder).first().prop('onChange')(e);
    expect(setEmailStub.calledWith(e.target.value)).to.equal(true);
  });

  it('given e.target.name = phone-input, onChange should call setPhone', () => {
    const { enzymeWrapper } = setup();
    const e = { target: { name: 'phone-input', value: 'value' } };
    enzymeWrapper.find(HCardBuilder).first().prop('onChange')(e);
    expect(setPhoneStub.calledWith(e.target.value)).to.equal(true);
  });

  it('given e.target.name = house-input, onChange should call setHouse', () => {
    const { enzymeWrapper } = setup();
    const e = { target: { name: 'house-input', value: 'value' } };
    enzymeWrapper.find(HCardBuilder).first().prop('onChange')(e);
    expect(setHouseStub.calledWith(e.target.value)).to.equal(true);
  });

  it('given e.target.name = street-input, onChange should call setHouse', () => {
    const { enzymeWrapper } = setup();
    const e = { target: { name: 'street-input', value: 'value' } };
    enzymeWrapper.find(HCardBuilder).first().prop('onChange')(e);
    expect(setStreetStub.calledWith(e.target.value)).to.equal(true);
  });

  it('given e.target.name = suburb-input, onChange should call setHouse', () => {
    const { enzymeWrapper } = setup();
    const e = { target: { name: 'suburb-input', value: 'value' } };
    enzymeWrapper.find(HCardBuilder).first().prop('onChange')(e);
    expect(setSuburbStub.calledWith(e.target.value)).to.equal(true);
  });

  it('given e.target.name = state-input, onChange should call setHouse', () => {
    const { enzymeWrapper } = setup();
    const e = { target: { name: 'state-input', value: 'value' } };
    enzymeWrapper.find(HCardBuilder).first().prop('onChange')(e);
    expect(setStateStub.calledWith(e.target.value)).to.equal(true);
  });

  it('given e.target.name = postcode-input, onChange should call setHouse', () => {
    const { enzymeWrapper } = setup();
    const e = { target: { name: 'postcode-input', value: 'value' } };
    enzymeWrapper.find(HCardBuilder).first().prop('onChange')(e);
    expect(setPostcodeStub.calledWith(e.target.value)).to.equal(true);
  });

  it('given e.target.name = country-input, onChange should call setHouse', () => {
    const { enzymeWrapper } = setup();
    const e = { target: { name: 'country-input', value: 'value' } };
    enzymeWrapper.find(HCardBuilder).first().prop('onChange')(e);
    expect(setCountryStub.calledWith(e.target.value)).to.equal(true);
  });
});
