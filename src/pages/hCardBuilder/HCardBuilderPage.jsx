import React from 'react';

import * as hooks from './hooks';
import defaultAvatar from '../../public/images/defaultAvatar.png';

import HCardBuilder from '../../common/components/hCardBuilder/HCardBuilder';
import HCardPreview from '../../common/components/hCardPreview/HCardPreview';

function HCardBuilderPage() {
  const classes = hooks.useStyles();
  const [name, setName] = hooks.useName('');
  const [surname, setSurname] = hooks.useSurname('');
  const [email, setEmail] = hooks.useEmail('');
  const [phone, setPhone] = hooks.usePhone('');
  const [house, setHouse] = hooks.useHouse('');
  const [street, setStreet] = hooks.useStreet('');
  const [suburb, setSuburb] = hooks.useSuburb('');
  const [state, setState] = hooks.useAddressState('');
  const [postcode, setPostcode] = hooks.usePostcode('');
  const [country, setCountry] = hooks.useCountry('');
  const [avatar, setAvatar] = hooks.useAvatar(defaultAvatar);

  const onChange = (e) => {
    switch (e.target.name) {
      case 'name-input': {
        setName(e.target.value);
        break;
      }
      case 'surname-input': {
        setSurname(e.target.value);
        break;
      }
      case 'email-input': {
        setEmail(e.target.value);
        break;
      }
      case 'phone-input': {
        setPhone(e.target.value);
        break;
      }
      case 'house-input': {
        setHouse(e.target.value);
        break;
      }
      case 'street-input': {
        setStreet(e.target.value);
        break;
      }
      case 'suburb-input': {
        setSuburb(e.target.value);
        break;
      }
      case 'state-input': {
        setState(e.target.value);
        break;
      }
      case 'postcode-input': {
        setPostcode(e.target.value);
        break;
      }
      case 'country-input': {
        setCountry(e.target.value);
        break;
      }
      default:
    }
  };

  const onUpload = (e) => {
    setAvatar(URL.createObjectURL(e.target.files[0]));
  };

  const onSave = () => {
    setName('');
    setSurname('');
    setEmail('');
    setPhone('');
    setHouse('');
    setStreet('');
    setSuburb('');
    setState('');
    setPostcode('');
    setCountry('');
    setAvatar('');
  };

  return (
    <div className={classes.root}>
      <div className={classes.builder}>
        <HCardBuilder
          onUpload={onUpload}
          onSave={onSave}
          onChange={onChange}
          name={name}
          surname={surname}
          email={email}
          phone={phone}
          house={house}
          street={street}
          suburb={suburb}
          state={state}
          postcode={postcode}
          country={country}
        />
      </div>
      <div className={classes.preview}>
        <HCardPreview
          name={name}
          surname={surname}
          email={email}
          phone={phone}
          house={house}
          street={street}
          suburb={suburb}
          state={state}
          postcode={postcode}
          country={country}
          avatar={avatar}
        />
      </div>
    </div>
  );
}

export default HCardBuilderPage;
