import { makeStyles } from '@material-ui/styles';
import { useState } from 'react';

export function useName(init) {
  const [name, setName] = useState(init);

  return [name, setName];
}

export function useSurname(init) {
  const [surname, setSurname] = useState(init);

  return [surname, setSurname];
}

export function useEmail(init) {
  const [email, setEmail] = useState(init);

  return [email, setEmail];
}

export function usePhone(init) {
  const [phone, setPhone] = useState(init);

  return [phone, setPhone];
}

export function useHouse(init) {
  const [house, setHouse] = useState(init);

  return [house, setHouse];
}

export function useStreet(init) {
  const [street, setStreet] = useState(init);

  return [street, setStreet];
}

export function useSuburb(init) {
  const [suburb, setSuburb] = useState(init);

  return [suburb, setSuburb];
}

export function useAddressState(init) {
  const [state, setState] = useState(init);

  return [state, setState];
}

export function usePostcode(init) {
  const [postcode, setPostcode] = useState(init);

  return [postcode, setPostcode];
}

export function useCountry(init) {
  const [country, setCountry] = useState(init);

  return [country, setCountry];
}

export function useAvatar(init) {
  const [avatar, setAvatar] = useState(init);

  return [avatar, setAvatar];
}
// eslint-disable-next-line import/prefer-default-export
export const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  builder: {
    display: 'flex',
    flexGrow: 0,
    maxWidth: 500,
  },
  preview: {
    display: 'flex',
    maxWidth: 500,
    flexGrow: 1,
    backgroundColor: '#e5e8eb',
    padding: '0 45px',
  },
}));
