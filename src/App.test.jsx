import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';

import Routes from './Routes';
import App from './App';

function setup(specProps) {
  const defaultProps = {
  };

  const props = {
    ...defaultProps,
    ...specProps,
  };
  const enzymeWrapper = shallow(<App {...props} />);

  return {
    props,
    enzymeWrapper,
  };
}

describe('App', () => {
  it('should render self and have Routes', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.first().is(Routes)).to.equal(true);
  });
});
